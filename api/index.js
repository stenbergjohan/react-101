var express = require("express");
var http = require("http");
var app = express();

const cors = require("cors");
app.use(cors());

const searchResults = [
  { name: "bing", keywords: ["bing", "search"], url: "https://bing.com" },
  { name: "google", keywords: ["google", "search"], url: "https://google.com" },
  { name: "facebook", keywords: ["facebook", "social-media"], url: "http://facebook.com" },
  { name: "mozilla", keywords: ["firefox", "browser"], url: "http://mozilla.org" },
];

function search(searchQuery) {
  return [
    ...new Set(
      searchResults
        .filter((sr) => sr.name.includes(searchQuery))
        .concat(searchResults.filter((sr) => sr.keywords.some((kw) => kw.includes(searchQuery))))
    ),
  ];
}

app.get("/search", function (req, res) {
  if (!req.query["query"]) {
    res.status(400).send("No search query was provided");
    return;
  }

  const query = decodeURIComponent(req.query["query"]);

  const result = search(query);

  res.send(result);
});

app.get("/ping", function (req, res) {
  res.send("pong");
});

http.createServer(app).listen(8080);
console.log("Server started");
