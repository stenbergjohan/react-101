import React from "react";
import "./Button.css";

interface IButtonProps {
  title: string;
  onClick: () => void;
}

function Button(props: IButtonProps) {
  return (
    <button className="search-button" title={props.title} onClick={props.onClick}>
      {props.title}
    </button>
  );
}

export { Button };
