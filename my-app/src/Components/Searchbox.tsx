import React from "react";
import "./Searchbox.css";

interface ISearchboxProps {
  text: string;
  setText: (s: string) => void;
}

function Searchbox(props: ISearchboxProps) {
  function handleKeyPress(event: React.ChangeEvent<HTMLInputElement>) {
    props.setText(event.target.value);
  }

  return <input className="searchbox" value={props.text} onChange={handleKeyPress}></input>;
}

export { Searchbox };
