import React from "react";
import "./Header.css";

interface IHeaderProps {
  text: string;
}

function Header(props: IHeaderProps) {
  return <h1 className="header">{props.text}</h1>;
}

export { Header };
