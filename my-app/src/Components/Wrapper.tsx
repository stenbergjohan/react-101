import React from "react";
import "./Wrapper.css";

interface IWrapperProps {
  children: React.ReactNode;
}

export function CenterView(props: IWrapperProps) {
  return <div className="wrapper">{props.children}</div>;
}
