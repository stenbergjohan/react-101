import "./App.css";
import React, { useState } from "react";
import { Header } from "./Components/Header";
import { Button } from "./Components/Button";
import { Searchbox } from "./Components/Searchbox";
import { CenterView } from "./Components/Wrapper";

function App() {
  const [text, setText] = useState("");

  function search() {
    alert(text);
  }

  return (
    <div className="app">
      <CenterView>
        <Header text="Search" />
        <Searchbox text={text} setText={setText} />
        <Button title="Search" onClick={search} />
      </CenterView>
    </div>
  );
}

export default App;
